//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/

	{"", "sb-music",	5,		 10},

	{"", "sb-price btc bitcoin BTC",	150,		 0},
	{"", "sb-price xtz tezos XTZ",	        150,		 0},
	{"", "sb-price xmr monero XMR",	        150,		 0},

	{"", "sb-torrent",	30,		 0},
	{"", "sb-nettraf",	1,		15},
	{"", "sb-internet",	5,		15},
	{"", "sb-volume",	0,		10},

	{"", "sb-clock",	1,		 0},
	{"", "sb-battery",	2,		 0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = "  ";
static unsigned int delimLen = 5;
